from setuptools import setup

setup(
	name='carshare',
	packages=['carshare'],
	include_package_data=True,
	install_requires=[
		'flask',
	]
)
