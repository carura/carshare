import sys
import pytest

collect_ignore = ["carshare/venv"]

@pytest.fixture(scope="module")
def client(request):
    from carshare import app
    import carshare.config
    from carshare.database import db_session, init_db, destroy_db
    app.config.from_object('carshare.config.TestingConfig')
    client = app.test_client()
    with app.app_context():
        init_db()
        client.db_session = db_session
    yield client
    db_session.remove()
    destroy_db()
