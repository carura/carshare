from carshare.models import User
from carshare.views import load_user

def test_load_user_from_id(client):
    user = User('amanda', '2', '2', '2', '2', '2', '2')
    client.db_session.add(user)
    client.db_session.commit()
    assert user in client.db_session
    loaded_user = load_user(user.email)
    assert loaded_user.name == user.name
