from test_user import new_user, unique_user_data
from test_login import login
from carshare.models import Trip

def create_new_user_and_login(client):
    user_data = unique_user_data()
    user = new_user(client, user_data)
    login(client, user.email, user.password)
    return user

def test_user_can_add_single_trip(client):
    user = create_new_user_and_login(client)
    trip_data=dict(
        km_stand=123,
        sprit_stand=24,
        getankt=True,
        standort='Musterstr. 55'
    )
    response = client.post('/trip/new', data=trip_data)
    new_trip = client.db_session.query(Trip).filter_by(km_stand=trip_data['km_stand']).first()
    assert new_trip in client.db_session
    assert new_trip.driver == user.name
    assert b'Your trip has successfully been saved' in response.data
