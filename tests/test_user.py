import random
import string
from carshare.models import User
from carshare.forms import SignupForm
from test_login import login, logout

def random_string():
    return ''.join(random.choice(string.ascii_lowercase) for _ in range(6))

def unique_user_data():
    password = random_string()
    return dict(
        name=random_string(),
        email=random_string(),
        password=password,
        confirm=password,
        telefon='a number',
        adresse='an address',
        not_kontakt_name='contact name',
        not_kontakt_telefon='another telefon'
    )

def new_user(client, user_data):
    client.post('/signup', data=user_data)
    return client.db_session.query(User).filter_by(name=user_data['name']).first()

def test_add_user_using_signup_form(client):
    user_data = unique_user_data()
    user = new_user(client, user_data)
    assert user in client.db_session
    assert user.name == user_data['name']

def test_add_user_only_if_all_fields_present(client):
    user_data_no_email = unique_user_data()
    del user_data_no_email['email']
    user = new_user(client, user_data_no_email)
    assert user == None

def test_prompt_user_new_input_if_password_not_unique(client):
    user_data = unique_user_data()
    new_user(client, user_data)
    user_data_diff_name = user_data.copy()
    user_data_diff_name['name'] = 'user2'
    response_to_failed_try = client.post('/signup', data=user_data)
    user2 = client.db_session.query(User).filter_by(name=user_data_diff_name['name']).first()
    assert user2 == None
    assert b'Signup' in response_to_failed_try.data
    assert user_data_diff_name['email'].encode() in response_to_failed_try.data

def test_password_is_encrypted_in_database(client):
    user_data = unique_user_data()
    user = new_user(client, user_data)
    assert user.password != user_data['password']
    assert 'pbkdf2:sha256:50000' in user.password

def test_user_can_view_their_profile_when_logged_in(client):
    user_data = unique_user_data()
    user = new_user(client, user_data)
    login(client, user_data['email'], user_data['password'])
    response = client.get('/user/' + user_data['name'])
    assert user_data['name'].encode() in response.data

def test_user_can_edit_profile_when_logged_in(client):
    user_data = unique_user_data()
    user = new_user(client, user_data)
    login(client, user_data['email'], user_data['password'])

    edited_user_data = user_data.copy()
    edited_user_data['adresse'] = 'new address'
    edited_user_data['email'] = 'new email'
    edited_user_data['name'] = 'new_name'
    response = client.post('/user/' + user_data['name'] + '/edit', data=edited_user_data)
    assert b'Your information has been saved' in response.data
    assert edited_user_data['adresse'].encode() in response.data
    assert edited_user_data['email'].encode() in response.data

    logout(client)
    response_login = login(client, edited_user_data['email'], edited_user_data['password'])
    assert edited_user_data['name'].encode() in response_login.data
    assert b'You are logged in' in response_login.data
