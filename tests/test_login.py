from carshare.models import User

def login(client, email, password):
    return client.post('/login', data=dict(
        email=email,
        password=password
    ), follow_redirects=True)

def logout(client):
    return client.get('/logout', follow_redirects=True)

def test_registered_user_can_login(client):
    user = User('laura', 'hi@posteo.de', 'password', '23', '2', '2', '3')
    client.db_session.add(user)
    client.db_session.commit()
    assert user in client.db_session
    response = login(client, 'hi@posteo.de', 'password')
    assert b'Hi laura' in response.data
    assert b'You are logged in' in response.data

def test_user_can_logout(client):
    user = User('l', 'hi2@posteo.de', 'password3', '23', '2', '2', '3')
    client.db_session.add(user)
    client.db_session.commit()
    assert user in client.db_session
    login(client, 'hi2@posteo.de', 'password')
    response = logout(client)
    assert b'Hi ' not in response.data
    assert b'You are logged out' in response.data

def test_invalid_credentials_prompt_new_form(client):
    response = login(client, 'email_not_in_the_system', 'password')
    assert b'Invalid credentials. User does not exist.' in response.data
