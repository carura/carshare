import datetime
from test_trip import create_new_user_and_login
from test_user import unique_user_data
from carshare.models import Reservation

def test_user_can_reserve_car(client):
    user = create_new_user_and_login(client)
    reservation_data=dict(
        start='2008-03-04 09:30:00',
        end='2008-04-04 12:00:00'
    )
    response = client.post('/reservation/new', data=reservation_data)
    reservation = client.db_session.query(Reservation).filter_by(driver=user.name).first()
    assert reservation in client.db_session
    assert reservation.start == reservation_data['start']
