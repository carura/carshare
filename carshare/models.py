from sqlalchemy import Table, Column, Integer, String
from sqlalchemy.orm import mapper
from carshare.database import metadata, db_session
from werkzeug.security import generate_password_hash

class User(object):
    query = db_session.query_property()

    def __init__(self, name=None, email=None, password=None, telefon=None,
                 adresse=None, not_kontakt_name=None, not_kontakt_telefon=None):
        self.name = name
        self.email = email
        self.password = generate_password_hash(password)
        self.telefon = telefon
        self.adresse = adresse
        self.not_kontakt_name = not_kontakt_name
        self.not_kontakt_telefon = not_kontakt_telefon

    def __repr__(self):
        return '<User %r>' % (self.name)

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.email)

users = Table('users', metadata,
    Column('id', Integer, primary_key=True),
    Column('name', String(50), unique=True),
    Column('email', String(120), unique=True),
    Column('password', String(120), unique=True),
    Column('telefon', String(120)),
    Column('adresse', String(120)),
    Column('not_kontakt_name', String(50)),
    Column('not_kontakt_telefon', String(120)),
    extend_existing=True
)
mapper(User, users)

class Trip(object):
    query = db_session.query_property()

    def __init__(self, km_stand=None, sprit_stand=None, getankt=False,
                    standort=None, driver=None):
        self.km_stand = km_stand
        self.sprit_stand = sprit_stand
        self.getankt = getankt
        self.standort = standort
        self.driver = driver

    def __repr__(self):
        return '<Trip %r>' % (self.km_stand)

trips = Table('trips', metadata,
    Column('id', Integer, primary_key=True),
    Column('km_stand', Integer, unique=True),
    Column('sprit_stand', Integer),
    Column('getankt', String(20)),
    Column('standort', String(120)),
    Column('driver', String(120)),
    extend_existing=True
)
mapper(Trip, trips)

class Reservation(object):
    query = db_session.query_property()

    def __init__(self, start=None, end=None, driver=None):
        self.start = start
        self.end = end
        self.driver = driver

reservations = Table('reservations', metadata,
    Column('id', Integer, primary_key=True),
    Column('start', String(50), unique=True),
    Column('end', String(50), unique=True),
    Column('driver', String(120)),
    extend_existing=True
)
mapper(Reservation, reservations)
