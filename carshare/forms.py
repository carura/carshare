from wtforms import Form, StringField, PasswordField, validators, DateTimeField
from wtforms.validators import InputRequired

class UserBaseForm(Form):
    name = StringField('Name', [validators.InputRequired()])
    email = StringField('Email Address', [validators.InputRequired()])
    telefon = StringField('Telefon', [validators.InputRequired()])
    adresse = StringField('Adresse', [validators.InputRequired()])
    not_kontakt_name = StringField('Not Kontakt Name', [validators.InputRequired()])
    not_kontakt_telefon = StringField('Not Kontakt Telefon', [validators.InputRequired()])
    confirm = PasswordField('Repeat Password')

class SignupForm(UserBaseForm):
    password = PasswordField('New Password', [
        validators.InputRequired(),
        validators.EqualTo('confirm', message='Passwords must match')
    ])

class LoginForm(Form):
    email = StringField('Email Address', [validators.InputRequired()])
    password = PasswordField('Password', [validators.InputRequired()])

class EditProfileForm(UserBaseForm):
    password = PasswordField('New Password', [
        validators.EqualTo('confirm', message='Passwords must match')
    ])

class TripForm(Form):
    km_stand = StringField('Kilometer Stand', [validators.InputRequired()])
    sprit_stand = StringField('Sprit Stand', [validators.InputRequired()])
    getankt = StringField('Getankt', [validators.InputRequired()])
    standort = StringField('Standort', [validators.InputRequired()])

class ReservationForm(Form):
    start = DateTimeField('Start', [validators.InputRequired()], format='%Y-%m-%d %H:%M:%S')
    end = DateTimeField('End', [validators.InputRequired()], format='%Y-%m-%d %H:%M:%S')
