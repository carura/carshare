from flask import Flask
import os
import carshare.config
from flask_login import LoginManager

config = {
    "development": "carshare.config.DevelopmentConfig",
    "testing": "carshare.config.TestingConfig",
    "default": "carshare.config.DevelopmentConfig"
}

config_name = os.getenv('FLASK_CONFIGURATION', 'default')

app = Flask(__name__)
app.config.from_object(config[config_name])

login_manager = LoginManager()
login_manager.init_app(app)

import carshare.views
from carshare.database import db_session, init_db

@app.cli.command('initdb')
def initdb_command():
    init_db()
    print('Initialized the database.')

@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()
