class BaseConfig(object):
    DEBUG = False
    TESTING = False
    SECRET_KEY = '\xaaR\x14\xf9s/\xac\x9b\x98\xd9Q\xf6\xb0i\x86BZ\xb5\x08\x8b<\xbb1\xdd'

class DevelopmentConfig(BaseConfig):
    DEBUG = True
    TESTING = True
    SQLALCHEMY_DATABASE_URI=('sqlite:////tmp/carshare.db')

class TestingConfig(BaseConfig):
    DEBUG = False
    TESTING = True
    SQLALCHEMY_DATABASE_URI=('sqlite:////tmp/carshare_test.db')
