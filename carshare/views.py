from flask import render_template, request, flash, redirect, url_for
from carshare import app, login_manager
from carshare.models import User, Trip, Reservation
from carshare.forms import SignupForm, LoginForm, EditProfileForm, TripForm, ReservationForm
from carshare.database import db_session
from sqlalchemy import exc
from flask_login import login_user, logout_user, login_required, current_user

@login_manager.user_loader
def load_user(email):
    return User.query.filter_by(email=email).first()

@app.route('/')
def home():
    return render_template('home.html')

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    form = SignupForm(request.form)
    if request.method == 'POST' and form.validate():
        user = User(form.name.data, form.email.data, form.password.data,
                        form.telefon.data, form.adresse.data,
                        form.not_kontakt_name.data, form.not_kontakt_telefon.data)
        try:
            db_session.add(user)
            db_session.commit()
            flash('You have been successfully registered')
            return redirect(url_for('home'))
        except exc.IntegrityError:
            db_session.rollback()
    return render_template('signup.html', form=form)

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm(request.form)
    if request.method == 'POST' and form.validate():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            login_user(user)
            flash('You are logged in')
            return render_template('home.html')
        else:
            form.email.errors.append('Invalid credentials. User does not exist.')
    return render_template('login.html', form=form)

@app.route('/logout')
def logout():
    logout_user()
    return render_template('home.html')

@app.route('/user/<username>')
@login_required
def show_user_profile(username):
    user = User.query.filter_by(name=username).first()
    return render_template('show_user_profile.html', user=user)

@app.route('/user/<username>/edit', methods=['GET', 'POST'])
@login_required
def edit_user_profile(username):
    user = User.query.filter_by(name=username).first()
    form = EditProfileForm(request.form, user)
    if request.method == 'POST' and form.validate():
         form.populate_obj(user)
         db_session.add(user)
         db_session.commit()
         flash('Your information has been saved')
         return render_template('show_user_profile.html', user=user, form=form)
    return render_template('edit_user_profile.html', user=user, form=form)

@app.route('/trip/new', methods=['GET', 'POST'])
@login_required
def new_trip():
    form = TripForm(request.form)
    if request.method == 'POST' and form.validate():
        trip = Trip(form.km_stand.data, form.sprit_stand.data, form.getankt.data,
                    form.standort.data, current_user.name)
        try:
            db_session.add(trip)
            db_session.commit()
            flash('Your trip has successfully been saved')
            print(filter(lambda a: not a.startswith('__'), dir(trip)))
            return render_template('show_trip.html', trip=trip)
        except exc.IntegrityError:
            db_session.rollback()
    return render_template('new_object.html', form=form, title='New trip')

@app.route('/reservation/new', methods=['GET', 'POST'])
@login_required
def new_reservation():
    form = ReservationForm(request.form)
    if request.method == 'POST' and form.validate():
        reservation = Reservation(form.start.data, form.end.data, current_user.name)
        try:
            db_session.add(reservation)
            db_session.commit()
            flash('Your reservation was successful')
            return render_template('show_reservation.html', reservation=reservation)
        except exc.IntegrityError:
            db_session.rollback()
    return render_template('new_object.html', form=form, title='New reservation')
