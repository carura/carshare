# Carshare

## Installation (linux)

- Setup a virtual environment (venv) for Python3. Note: when you follow the normal Flask instructions, they will setup Python2 instead. You want Python3.5.

You should setup your 'venv' directory inside of your flask app directory. Follow the following instructions from home directory:
```
mkdir venv
python3 -m venv venv
```
Activate the virtual env:
```
. venv/bin/activate
```

Install requirements:
```
pip install -r requirements.txt
```
Documentation:
https://docs.python.org/3/library/venv.htm
http://flask.pocoo.org/docs/0.12/installation/#virtualenv

##  Database setup
Once you activate the virtual environment, run the following:
```
flask initdb
```
